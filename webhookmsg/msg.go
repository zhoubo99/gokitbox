package webhookmsg

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"gitee.com/zhoubo99/gokitbox/log"
)

type Msg interface {
	SendMsg(text string) error
}

type msgType string

const (
	//FeiShu 飞书
	FeiShu msgType = "FeiShu"
	// WeCom 企业微信
	WeCom msgType = "WeCom"
	// DingDing 钉钉
	DingDing msgType = "DingDing"
	// Telegram 电报
	Telegram msgType = "Telegram"
)

// NewMsg 生成消息结构体
func NewMsg(url string, m msgType) (Msg, error) {
	switch m {
	case FeiShu:
		return feiShuMsg{
			Url: url,
		}, nil
	case WeCom:
		return weComMsg{
			Url: url,
		}, nil
	case DingDing:
		return dingDingMsg{
			Url: url,
		}, nil
	case Telegram:
		return TelegramMsg{
			Url: url,
		}, nil

	default:
		return nil, fmt.Errorf("未定义的msg type:%v", m)
	}
}

// feiShu 飞书
type feiShuMsg struct {
	Url     string
	MsgType string `json:"msg_type"`
	Content fsText `json:"content"`
}

type fsText struct {
	Text string `json:"text"`
}

func (f feiShuMsg) SendMsg(text string) error {
	body := &feiShuMsg{
		MsgType: "text",
		Content: fsText{
			Text: text,
		},
	}

	byteBody, _ := json.Marshal(&body)
	readerBody := bytes.NewReader(byteBody)

	response, err := http.Post(f.Url, "application/json", readerBody)
	if err != nil {
		log.Errorf("发送消息失败!Err: %s", err.Error())
		return err
	}
	defer response.Body.Close()

	responseByte, _ := io.ReadAll(response.Body)
	log.Debugf("调用接口成功, response: %s", string(responseByte))

	return nil
}

// dingDing 钉钉
type dingDingMsg struct {
	Url string `json:"url"`
	// 钉钉需要关键字发送
	Constr  string       `json:"constr"`
	MsgType string       `json:"msgtype"`
	Text    dingDingText `json:"text"`
}

type dingDingText struct {
	Content string `json:"content"`
}

func (d dingDingMsg) SendMsg(text string) error {
	text = fmt.Sprintf("%s\n%s", d.Constr, text)
	body := &dingDingMsg{
		MsgType: "text",
		Text: dingDingText{
			Content: text,
		},
	}

	byteBody, _ := json.Marshal(&body)
	readerBody := bytes.NewReader(byteBody)

	response, err := http.Post(d.Url, "application/json", readerBody)
	if err != nil {
		log.Errorf("发送消息失败!Err: %s", err.Error())
		return err
	}
	defer response.Body.Close()

	responseByte, _ := io.ReadAll(response.Body)
	log.Debugf("调用接口成功, response: %s", string(responseByte))

	return nil
}

// weCom 企业微信 (同钉钉接口一致)
type weComMsg struct {
	Url     string    `json:"url"`
	MsgType string    `json:"msgtype"`
	Text    weComText `json:"text"`
}

type weComText struct {
	Content string `json:"content"`
}

func (w weComMsg) SendMsg(text string) error {
	body := &weComMsg{
		MsgType: "text",
		Text: weComText{
			Content: text,
		},
	}

	byteBody, _ := json.Marshal(&body)
	readerBody := bytes.NewReader(byteBody)

	response, err := http.Post(w.Url, "application/json", readerBody)
	if err != nil {
		log.Errorf("发送消息失败! Err: %v", err)
		return err
	}
	defer response.Body.Close()

	responseByte, _ := io.ReadAll(response.Body)
	log.Debugf("调用接口成功, response: %s", string(responseByte))

	return nil
}

// TelegramMsg 电报
type TelegramMsg struct {
	Url string `json:"url"` // get 请求(token直接放入url中)
}

func (t TelegramMsg) SendMsg(text string) error {

	encodeText := url.QueryEscape(text)

	response, err := http.Get(t.Url + encodeText)
	if err != nil {
		log.Errorf("发送消息失败 Err: %v", err)
		return err
	}

	defer response.Body.Close()

	responseByte, _ := io.ReadAll(response.Body)
	log.Debugf("调用接口成功, response: %s", string(responseByte))

	return nil
}
