#!/usr/bin/env bash

set -x

# 获取源码最近一次 git commit log，包含 commit sha 值，以及 commit message
GitCommitLog=`git log --pretty=oneline -n 1`
# 将 log 原始字符串中的单引号替换成双引号
GitCommitLog=${GitCommitLog//\'/\"}
# git user
GitUser=`git config user.name`
# git email
GitEmail=`git config user.email`
# 获取当前时间
BuildTime=`date +'%Y-%m-%d %H:%M:%S'`
# 获取 Go 的版本
BuildGoVersion=`go version`


# 将以上变量序列化至 LDFlags 变量中
LDFlags=" \
    -X 'gitee.com/zhoubo99/gokitbox/buildinfo.GitCommitLog=${GitCommitLog}' \
    -X 'gitee.com/zhoubo99/gokitbox/buildinfo.GitUser=${GitUser}' \
    -X 'gitee.com/zhoubo99/gokitbox/buildinfo.GitEmail=${GitEmail}' \
    -X 'gitee.com/zhoubo99/gokitbox/buildinfo.BuildTime=${BuildTime}' \
    -X 'gitee.com/zhoubo99/gokitbox/buildinfo.BuildGoVersion=${BuildGoVersion}' \

"


## 编译
go build -ldflags "$LDFlags"