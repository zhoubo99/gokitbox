package buildinfo

/*

参考: http://t.csdnimg.cn/IDnNd

*/

import (
	"fmt"
	"runtime"
)

var (
	// GitCommitLog 初始化为 unknown，如果编译时没有传入这些值，则为 unknown
	GitCommitLog   = "unknown"
	GitUser        = "unknown"
	GitEmail       = "unknown"
	BuildTime      = "unknown"
	BuildGoVersion = "unknown"
)

func ShowBuildInfo() {

	fmt.Printf(`
/-----------------------------BUILD INFO-----------------------------
GitCommitLog: %s
GitUser: %s
GitEmail: %s
BuildTime: %s
GoVersion: %s
Runtime: %s/%s
---------------------------------------------------------------------/
`,
		GitCommitLog, GitUser, GitEmail, BuildTime, BuildGoVersion, runtime.GOOS, runtime.GOARCH)

}
