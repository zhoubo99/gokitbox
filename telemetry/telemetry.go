package telemetry

import (
	"context"
	"fmt"
	"os"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
)

type Config struct {
	OtelAddr    string
	MetricsAddr string

	ServiceName       string
	ServiceVersion    string
	ServiceInstanceID string
}

var cfg Config // global config

func (c Config) WithFromEnv() Config {
	f := func(s *string, env string) {
		if *s != "" {
			return
		}
		*s = os.Getenv(env)
	}
	f(&c.OtelAddr, "OTEL_ADDR")
	f(&c.MetricsAddr, "METRICS_ADDR")
	f(&c.ServiceName, "SERVICE_NAME")
	f(&c.ServiceVersion, "SERVICE_VERSION")
	f(&c.ServiceInstanceID, "SERVICE_INSTANCE_ID")
	return c
}

func (c Config) WithDefault() Config {
	if c.MetricsAddr == "" {
		c.MetricsAddr = ":28080"
	}
	if c.ServiceVersion == "" {
		c.ServiceVersion = "0.0.0"
	}
	return c
}

// TODO 如需上报,请打开该函数注释部分
func Setup(c Config) error {
	ctx := context.Background()
	c = c.WithDefault()
	cfg = c

	res, err := resource.New(ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(c.ServiceName),
			semconv.ServiceVersionKey.String(c.ServiceVersion),
			semconv.ServiceInstanceIDKey.String(c.ServiceInstanceID),
		),
	)

	if err != nil {
		return fmt.Errorf("failed to create resource: %w", err)
	}
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()
	//conn, err := grpc.DialContext(ctx, c.OtelAddr,
	//	// Note the use of insecure transport here. TLS is recommended in production.
	//	grpc.WithTransportCredentials(insecure.NewCredentials()),
	//	grpc.WithBlock(),
	//)
	//if err != nil {
	//	return fmt.Errorf("failed to create gRPC connection to collector: %w", err)
	//}

	//traceExporter, err := otlptracegrpc.New(ctx, otlptracegrpc.WithGRPCConn(conn))
	//if err != nil {
	//	return fmt.Errorf("failed to create trace exporter: %w", err)
	//}

	//bsp := sdktrace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		//sdktrace.WithSpanProcessor(bsp),
	)
	otel.SetTracerProvider(tracerProvider)

	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	return nil
}
