package daemon

import (
	"os"
	"os/exec"
	"strconv"

	"gitee.com/zhoubo99/gokitbox/log"
	"go.uber.org/zap"
)

func PidIsNotExist(file string) bool {
	pid, err := os.ReadFile(file)
	if err != nil {
		return true
	}

	p, err := strconv.Atoi(string(pid))
	if err != nil {
		return true
	}

	pidFile := "/proc/" + strconv.Itoa(p)
	_, err = os.ReadDir(pidFile)
	return os.IsNotExist(err)

}

func Daemon() {
	app := os.Args[0]
	args := []string{"-is_daemon"}
	if len(os.Args) > 1 {
		for _, v := range os.Args[1:] {
			if v == "-d" {
				continue
			}
			args = append(args, v)
		}
	}

	pidFile := app + ".pid"
	if !PidIsNotExist(pidFile) {
		log.Infof(" %s startd skip.", app)
		os.Exit(0)
	}

	log.InitReleaseLogger(app)
	log.Flush()

	log.Info("start", zap.String("cmd", app), zap.Any("args", args))

	cmd := exec.Command(app, args...)
	if err := cmd.Start(); err != nil {
		log.Errorf("start %s failed, error: %s", app, err.Error())
		os.Exit(99)
	}
	pid := cmd.Process.Pid
	log.Infof("%s [PID] %d running...", app, pid)

	os.WriteFile(pidFile, []byte(strconv.Itoa(pid)), 0700)

	os.Exit(0)
}
