package gokitbox

import (
	"flag"
)

// FlagGoDaemon 是否后台运行
var FlagGoDaemon bool

// FlagGoBuild build 信息
var FlagGoBuild bool

// IsDaemon 已后台运行标志
var IsDaemon bool

func init() {
	flag.BoolVar(&FlagGoDaemon, "d", false, "run app as a daemon with -d=true.")
	flag.BoolVar(&FlagGoBuild, "v", false, "show build info -v=true.")
	flag.BoolVar(&IsDaemon, "is_daemon", false, "for marking purposes only, do not use")
}
