package log

import (
	"io"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type levelEnablerFunc func(zapcore.Level) bool

type TeeOption struct {
	Out io.Writer
	levelEnablerFunc
}

func newReleaseEncodingConfig() zapcore.Encoder {
	return zapcore.NewJSONEncoder(zapcore.EncoderConfig{
		MessageKey:     "msg",
		LevelKey:       "level",
		TimeKey:        "ts",
		NameKey:        "logger",
		CallerKey:      "caller",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.CapitalLevelEncoder,
		EncodeTime:     timeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	})
}

// NewTee 根据日志级别写入多个输出
// https://pkg.go.dev/go.uber.org/zap#example-package-AdvancedConfiguration
func NewTee(tees []TeeOption, opts ...zap.Option) *zap.Logger {
	var cores []zapcore.Core
	for _, tee := range tees {
		core := zapcore.NewCore(
			newReleaseEncodingConfig(),
			zapcore.AddSync(tee.Out),
			zap.LevelEnablerFunc(tee.levelEnablerFunc),
		)
		cores = append(cores, core)
	}
	return zap.New(zapcore.NewTee(cores...), opts...)
}
