package log

import (
	"encoding/json"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type jsonStringer struct {
	v interface{}
}

func (s jsonStringer) String() string {
	b, _ := json.Marshal(s.v)
	return string(b)
}

func JSONField(key string, v interface{}) zap.Field {
	return zap.Stringer(key, jsonStringer{v: v})
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
}

func milliSecondsDurationEncoder(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendFloat64(float64(d) / float64(time.Millisecond))
}
