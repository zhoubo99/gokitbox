package log

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	KeyRequestID string = "requestID"
	KeyUsername  string = "username"
)

// zapLogger is a logr.Logger that uses Zap to log.
type zapLogger struct {
	zapLogger *zap.Logger
	infoLogger
}

type infoLogger struct {
	level zapcore.Level
	log   *zap.Logger
}

func (z *zapLogger) Debug(msg string, fields ...zap.Field) {
	z.zapLogger.Debug(msg, fields...)
}

func (z *zapLogger) Debugf(format string, v ...interface{}) {
	z.zapLogger.Sugar().Debugf(format, v...)
}

func (z *zapLogger) Debugw(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Debugw(msg, keysAndValues...)
}

func (z *zapLogger) Info(msg string, fields ...zap.Field) {
	z.zapLogger.Info(msg, fields...)
}

func (z *zapLogger) Infof(format string, v ...interface{}) {
	z.zapLogger.Sugar().Infof(format, v...)
}

func (z *zapLogger) Infow(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Infow(msg, keysAndValues...)
}

func (z *zapLogger) Warn(msg string, fields ...zap.Field) {
	z.zapLogger.Warn(msg, fields...)
}

func (z *zapLogger) Warnf(format string, v ...interface{}) {
	z.zapLogger.Sugar().Warnf(format, v...)
}

func (z *zapLogger) Warnw(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Warnw(msg, keysAndValues...)
}

func (z *zapLogger) Error(msg string, fields ...zap.Field) {
	z.zapLogger.Error(msg, fields...)
}

func (z *zapLogger) Errorf(format string, v ...interface{}) {
	z.zapLogger.Sugar().Errorf(format, v...)
}

func (z *zapLogger) Errorw(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Errorw(msg, keysAndValues...)
}

func (z *zapLogger) Panic(msg string, fields ...zap.Field) {
	z.zapLogger.Panic(msg, fields...)
}

func (z *zapLogger) Panicf(format string, v ...interface{}) {
	z.zapLogger.Sugar().Panicf(format, v...)
}

func (z *zapLogger) Panicw(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Panicw(msg, keysAndValues...)
}

func (z *zapLogger) Fatal(msg string, fields ...zap.Field) {
	z.zapLogger.Fatal(msg, fields...)
}

func (z *zapLogger) Fatalf(format string, v ...interface{}) {
	z.zapLogger.Sugar().Fatalf(format, v...)
}

func (z *zapLogger) Fatalw(msg string, keysAndValues ...interface{}) {
	z.zapLogger.Sugar().Fatalw(msg, keysAndValues...)
}

func (z *zapLogger) L(ctx context.Context) *zapLogger {
	lg := z.clone()

	requestID, _ := ctx.Value(KeyRequestID).(string)
	username, _ := ctx.Value(KeyUsername).(string)
	lg.zapLogger = lg.zapLogger.With(zap.String(KeyRequestID, requestID), zap.String(KeyUsername, username))

	return lg
}

func (z *zapLogger) V(level int) InfoLogger {
	lvl := zapcore.Level(-1 * level)
	if z.zapLogger.Core().Enabled(lvl) {
		return &infoLogger{
			level: lvl,
			log:   z.zapLogger,
		}
	}

	return disabledInfoLogger
}

func (z *zapLogger) Write(p []byte) (n int, err error) {
	z.zapLogger.Info(string(p))

	return len(p), nil
}

func (z *zapLogger) WithValues(keysAndValues ...interface{}) Logger {
	newLogger := z.zapLogger.With(handleFields(z.zapLogger, keysAndValues)...)

	return NewLogger(newLogger)
}

func (z *zapLogger) WithName(name string) Logger {
	newLogger := z.zapLogger.Named(name)

	return NewLogger(newLogger)
}

func (z *zapLogger) WithContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, logContextKey, z)
}

func (z *zapLogger) Flush() {
	_ = z.zapLogger.Sync()
}

func (z *zapLogger) clone() *zapLogger {
	logger := *z

	return &logger
}

var _ Logger = &zapLogger{}
var disabledInfoLogger = &noopInfoLogger{}

func NewLogger(l *zap.Logger) Logger {
	return &zapLogger{
		zapLogger: l,
		infoLogger: infoLogger{
			log:   l,
			level: zap.InfoLevel,
		},
	}
}

func (l *infoLogger) Enabled() bool { return true }
func (l *infoLogger) Info(msg string, fields ...zap.Field) {
	if checkedEntry := l.log.Check(l.level, msg); checkedEntry != nil {
		checkedEntry.Write(fields...)
	}
}

func (l *infoLogger) Infof(format string, args ...interface{}) {
	if checkedEntry := l.log.Check(l.level, fmt.Sprintf(format, args...)); checkedEntry != nil {
		checkedEntry.Write()
	}
}

func (l *infoLogger) Infow(msg string, keysAndValues ...interface{}) {
	if checkedEntry := l.log.Check(l.level, msg); checkedEntry != nil {
		checkedEntry.Write(handleFields(l.log, keysAndValues)...)
	}
}

type noopInfoLogger struct{}

func (l *noopInfoLogger) Enabled() bool                    { return false }
func (l *noopInfoLogger) Info(_ string, _ ...zap.Field)    {}
func (l *noopInfoLogger) Infof(_ string, _ ...interface{}) {}
func (l *noopInfoLogger) Infow(_ string, _ ...interface{}) {}

//func NewTraceLogger(ctx context.Context) Logger {
//	_ = telemetry.Setup(telemetry.Config{})
//	span := trace.SpanFromContext(ctx)
//	traceId := span.SpanContext().TraceID().String()
//	return FromContext(ctx).WithValues("trace_id", traceId)
//}
