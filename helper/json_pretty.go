package helper

import (
	"bytes"
	"encoding/json"
)

func JsonPretty(data []byte) (result string, err error) {

	var out bytes.Buffer
	err = json.Indent(&out, data, "", "\t")
	result = out.String()
	return
}
