package listfiles

import (
	"os"
	"strings"
)

// DelTailRoot 删除路径后面的/符号
func DelTailRoot(t []byte) string {
	if t[len(t)-1] == '/' {
		t[len(t)-1] = ' '
	}

	return string(t)
}

// ListFiles 目录下的所有文件
// cur 是否包含当前目录
func ListFiles(_path string, cur bool) ([]string, error) {

	path := DelTailRoot([]byte(_path))
	// Remove the trailing path separator if dirname has.
	dir := strings.TrimSuffix(path, string(os.PathSeparator))

	infos, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	paths := make([]string, 0, len(infos))

	// Include current directory.
	if cur {
		paths = append(paths, dir)
	}

	for _, info := range infos {
		path := dir + string(os.PathSeparator) + info.Name()
		if info.IsDir() {
			tmp, err := ListFiles(path, cur)
			if err != nil {
				return nil, err
			}
			paths = append(paths, tmp...)
			continue
		}
		paths = append(paths, path)
	}
	return paths, nil
}
